
//!//
let servicesNav = document.querySelectorAll('.our-services-nav-item');
let servicesContent = document.querySelectorAll('.our-services-content-item');
servicesNav.forEach(item  => {
    item.addEventListener('click', () => {
        document.querySelector('.active').classList.remove('active');
        item.classList.add('active');
        servicesContent.forEach(contentItem => {
            console.log(contentItem.dataset.content,  item.dataset.content)
            if(contentItem.dataset.content !== item.dataset.content) {
                contentItem.classList.remove('active')
            } else {
                contentItem.classList.add('active')
            }
        })
    })
})

let loadMoreBtn = document.querySelector('.load-more-btn');
let worksContent = document.querySelectorAll('.our-works-item');
let worksNav = document.querySelector('.our-works-nav')
loadMoreBtn.addEventListener('click', (event) => {
    worksContent.forEach(item => {
        if(item.classList.contains('hidden')) {
            item.classList.remove('hidden')
        }
    })
    loadMoreBtn.style.display = 'none'
})
worksNav.addEventListener('click', function (event) {

    worksContent.forEach(item => {
        if(item.dataset.category === event.target.dataset.category) {
            item.classList.remove('hidden')
        } else {
            item.classList.add('hidden')
        }
        if(event.target.classList.contains('all')) {
            item.classList.remove('hidden')
            loadMoreBtn.style.display = 'none'
        }
    })
})
let slideIndex = 1;
function slider(slide) {
    let number;
    let sliderContent = document.querySelectorAll('.feedback');
    let sliderNav = document.querySelectorAll('.feedback-slider-item');

    if(slide > sliderContent.length) {
        slideIndex = 1;
    }
    if(slide < 1) {
        slideIndex = sliderContent.length;
    }

    for(number = 0; number < sliderContent.length; number++) {
        sliderContent[number].classList.remove('feedback-active')
    }
    sliderContent[slideIndex-1].classList.add('feedback-active');

    for(number = 0; number < sliderNav.length; number++) {
        sliderNav[number].classList.remove('slider-animation');
    }
    sliderNav[slideIndex-1].classList.add('slider-animation')
}

let arrowBtnRight = document.querySelector('#right');
arrowBtnRight.addEventListener('click', nextSlide)
function nextSlide() {
    slider(slideIndex += 1)
}

let arrowBtnLeft = document.querySelector('#left');
arrowBtnLeft.addEventListener('click', previousSlide)
function previousSlide() {
    slider(slideIndex -= 1)
}
function sliderNum(index) {
    slider(slideIndex = index)
    console.log(slideIndex)
}

